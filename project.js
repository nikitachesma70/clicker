let money = 15000000;
let passiveEarn = 0;
let buyedCreaturesCount = {
    peasant: 0,
    hut: 0,
    warrior: 0,
    fortress: 0,
    wizard: 0,
    tower: 0
};
let shopCreaturesInfo = {
    peasant:{
        cost: 15,
        profit: 1
    },
    hut:{
        cost: 100,
        profit: 5
    },
    warrior:{
        cost: 500,
        profit: 10
    },
    fortress:{
        cost: 3000,
        profit: 50
    },
    wizard: {
        cost: 10000,
        profit: 80
    },
    tower: {
        cost: 100000,
        profit: 150
    }
};
function firework(){
    let count = 200;
    let defaults = {
        origin: { y: 0.7 }
    };
    function fire(particleRatio, opts) {
    confetti(Object.assign({}, defaults, opts, {
        particleCount: Math.floor(count * particleRatio)
    }));
    }

    fire(0.25, {
        spread: 26,
        startVelocity: 55,
    });
    fire(0.2, {
        spread: 60,
    });
    fire(0.35, {
        spread: 100,
        decay: 0.91,
        scalar: 0.8
    });
    fire(0.1, {
        spread: 120,
        startVelocity: 25,
        decay: 0.92,
        scalar: 1.2
    });
    fire(0.1, {
        spread: 120,
        startVelocity: 45,
    });
}

function createTable(name,top=0)
{

    let table_div = document.createElement('div');
    table_div.setAttribute('id', name + "_" + "table");

    table_div.style.position = "absolute";
    table_div.style.top =  80*top + 10*(top+1) + "px";
    table_div.style.marginLeft = "50px";
    table_div.style.marginRight = "392px";

    table_div.style.height = "80px";
    table_div.style.border = "2px solid #389c51";
    table_div.style.overflowY = "scroll";

    document.getElementById("map").appendChild(table_div);
}

function putItem(name){
    let item = document.createElement('img');
    item.src = document.getElementById(name).src;
    item.style.position="relative";
    item.style.width = "40px";

    document.getElementById(name + "_table").appendChild(item);
}


function buyItem(cost, earn){
    if(money >= cost){
        money = money - cost;
        document.getElementById("text").dispatchEvent(new Event("click"))
        passiveEarn = passiveEarn + earn;
        return true;
    }  
    return false;  
}


function fromCostToPriceList(price,div){
    let p = (price-price%(div/1000))/div;
    return p;
}

function priceList(price){
    if(price<1000){
        return price;
    }
    else if(1000 <= price && price < 1e6 ){
        return fromCostToPriceList(price,1000) + " thousand";
    }
    else if(1e6 <= price && price < 1e9){
        return fromCostToPriceList(price,1e6) + " million";
    }
    else if(1e9 <= price){
        return fromCostToPriceList(price,1e9) + " billion";
    }
}
function createShopPrice(name, shopCreaturesItem, cost){
    let peasantCost = document.createElement("p");
    peasantCost.className = "priceList";
    peasantCost.setAttribute("id", name);
    peasantCost.textContent = cost;
    peasantCost.setAttribute("shopCreaturesItem", shopCreaturesItem)
    function changePrice(){ 
        let new_cost = eval("shopCreaturesInfo."+shopCreaturesItem+".cost") 
        peasantCost.textContent = priceList(new_cost);
        console.log("new cost", new_cost);
    }
    peasantCost.addEventListener("click",changePrice,false);
    return peasantCost;
}
function createShopItem(image, alt, id){
    let peasant = document.createElement('img');
    peasant.src = image;
    peasant.addEventListener('load', LoadPeasant, false);
    function LoadPeasant() { 
        peasant.setAttribute('alt', alt);
        peasant.setAttribute('id', id);
        peasant.className = "imageShop";
        
    }
    return peasant;
}

function createPeasant(){
    let peasant_div = document.createElement('div');
    peasant_div.style.right = "50px";
    peasant_div.style.top = "0px";
    peasant_div.style.position = "absolute";

    let peasantCost = createShopPrice("peasant_price", "peasant", shopCreaturesInfo.peasant.cost);
    peasant_div.appendChild(peasantCost);
    let peasant = createShopItem('images/peasant.png','Peasant','peasant');
    peasant.addEventListener('click', addCountUp, false);
        function addCountUp(){
            if(buyItem(shopCreaturesInfo.peasant.cost, shopCreaturesInfo.peasant.profit)){

                if(!buyedCreaturesCount.peasant)
                {
                    createTable('peasant',0);
                }
                    buyedCreaturesCount.peasant = buyedCreaturesCount.peasant + 1;
                    shopCreaturesInfo.peasant.cost = shopCreaturesInfo.peasant.cost + buyedCreaturesCount.peasant;
                    console.log(shopCreaturesInfo.peasant, buyedCreaturesCount.peasant);
                    document.getElementById("peasant_price").dispatchEvent(new Event("click"));
                    
                    putItem('peasant');
            }  
        }


    peasant_div.appendChild( peasant);
    return peasant_div;
}
function createHut(){
    let hut_div = document.createElement('div');
    hut_div.style.right = "50px";
    hut_div.style.top = "115px";
    hut_div.style.position = "absolute";    
    
    let hut = createShopItem('images/hut.png','Hut','hut');
        hut.addEventListener('click', addCountUp, false);
        function addCountUp(){
            if(buyItem(shopCreaturesInfo.hut.cost, shopCreaturesInfo.hut.profit)){

                if(!buyedCreaturesCount.hut)
                {
                    createTable('hut',1);
                }

                buyedCreaturesCount.hut = buyedCreaturesCount.hut + 1;
                shopCreaturesInfo.hut.cost = shopCreaturesInfo.hut.cost + buyedCreaturesCount.hut;
                document.getElementById("hut_price").dispatchEvent(new Event("click"));
                putItem('hut');
            }
        }
    
    let hutCost = createShopPrice("hut_price", "hut", shopCreaturesInfo.hut.cost);
    hut_div.appendChild(hutCost);

    hut_div.appendChild(hut);
    return hut_div;
}
function createWarrior(){
    let warrior_div = document.createElement('div');
    warrior_div.style.right = "50px";
    warrior_div.style.top = "230px";
    warrior_div.style.position = "absolute";    
    
    let warrior = createShopItem('images/warrior.png','Warrior','warrior');
    warrior.addEventListener('click', addCountUp, false);
    function addCountUp(){
        if(buyItem(shopCreaturesInfo.warrior.cost, shopCreaturesInfo.warrior.profit)){
            
            if(!buyedCreaturesCount.warrior)
                {
                    createTable('warrior',2);
                }

            buyedCreaturesCount.warrior = buyedCreaturesCount.warrior + 1;
            shopCreaturesInfo.warrior.cost = shopCreaturesInfo.warrior.cost + buyedCreaturesCount.warrior;
            document.getElementById("warrior_price").dispatchEvent(new Event("click"));
                putItem("warrior");
        }   
    }
    let warriorCost = createShopPrice("warrior_price", "warrior", shopCreaturesInfo.warrior.cost);
    warrior_div.appendChild(warriorCost);

    warrior_div.appendChild(warrior);
    return warrior_div;
}
function createFortress(){
    let fortress_div = document.createElement('div');
    fortress_div.style.right = "50px";
    fortress_div.style.top = "345px";
    fortress_div.style.position = "absolute";    
    
    let fortress = createShopItem('images/fortress.png','Fortress','fortress');
    fortress.addEventListener('click', addCountUp, false);
    function addCountUp(){
        if(buyItem(shopCreaturesInfo.fortress.cost, shopCreaturesInfo.fortress.profit)){
           
            if(!buyedCreaturesCount.fortress)
                {
                    createTable("fortress",3);
                }
           
            buyedCreaturesCount.fortress = buyedCreaturesCount.fortress + 1;
            shopCreaturesInfo.fortress.cost = shopCreaturesInfo.fortress.cost + buyedCreaturesCount.fortress;
            document.getElementById("fortress_price").dispatchEvent(new Event("click"));
            putItem("fortress");
        }   
    }
    
    let fortressCost = createShopPrice("fortress_price", "fortress", shopCreaturesInfo.fortress.cost);
    fortress_div.appendChild(fortressCost);
    fortress_div.appendChild(fortress);
    return fortress_div;
}
function createWizard(){
    let wizard_div = document.createElement('div');
    wizard_div.style.right = "50px";
    wizard_div.style.top = "460px";
    wizard_div.style.position = "absolute";    
    
    let wizard = createShopItem('images/wizard.png','Wizard','wizard');
    wizard.addEventListener('click', addCountUp, false);
    function addCountUp(){
        if(buyItem(shopCreaturesInfo.wizard.cost, shopCreaturesInfo.wizard.profit)){
            
            if(!buyedCreaturesCount.wizard)
                {
                    createTable("wizard",4);
                }
            
            buyedCreaturesCount.wizard = buyedCreaturesCount.wizard + 1;
            shopCreaturesInfo.wizard.cost = shopCreaturesInfo.wizard.cost + buyedCreaturesCount.wizard;
            document.getElementById("wizard_price").dispatchEvent(new Event("click"));
            putItem("wizard");
        }  
    }
    
    let wizardCost = createShopPrice("wizard_price", "wizard", shopCreaturesInfo.wizard.cost);
    wizard_div.appendChild(wizardCost);
    wizard_div.appendChild(wizard);
    return wizard_div;
}
function createTower(){
    let tower_div = document.createElement('div');
    tower_div.style.right = "50px";
    tower_div.style.top = "575px";
    tower_div.style.position = "absolute";    
    
    let tower= createShopItem('images/tower.png','Tower','tower');
    tower.addEventListener('click', addCountUp, false);
    function addCountUp(){            
        if(buyItem(shopCreaturesInfo.tower.cost, shopCreaturesInfo.tower.profit)){
            if (buyedCreaturesCount.tower==0){
                firework();
            }
            
            if(!buyedCreaturesCount.tower)
                {
                    createTable("tower",5);
                }

            buyedCreaturesCount.tower = buyedCreaturesCount.tower + 1;
            shopCreaturesInfo.tower.cost = shopCreaturesInfo.tower.cost + buyedCreaturesCount.tower;
            document.getElementById("tower_price").dispatchEvent(new Event("click"));
            console.log(shopCreaturesInfo.tower.cost);
            putItem("tower");
        }      
    }
    
    let towerCost = createShopPrice("tower_price", "tower", shopCreaturesInfo.tower.cost);
    tower_div.appendChild(towerCost);
    tower_div.appendChild(tower);    
    return tower_div;
}
function createShop(){
    let shop_div = document.createElement('div');
    shop_div.style.overflowY = "scroll";
    shop_div.style.width = "342px";
    shop_div.style.float = "right";
    shop_div.className = "side-blocks";
    shop_div.appendChild(createPeasant());
    shop_div.appendChild(createHut());
    shop_div.appendChild(createWarrior());
    shop_div.appendChild(createFortress());
    shop_div.appendChild(createWizard());
    shop_div.appendChild(createTower());
    return shop_div;
}
function coins(){
    let duration = 2000;
    let animationEnd = Date.now() + duration;
    let skew = 1;
    let min = 0;
    let max = 342/window.innerWidth; // подобрать под размер экрана!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    function randomInRange(min, max) {
        return Math.random() * (max - min) + min;
    }
        
    (function frame() {
        let timeLeft = animationEnd - Date.now();
        let ticks = Math.max(200, 500 * (timeLeft / duration));
        skew = Math.max(0.8, skew - 0.001);
    
        confetti({
            particleCount: 1,
            startVelocity: 0,
            ticks: ticks,
            gravity: 0.5,
            origin: {
                x: Math.random()*(max - min) + min,
                y: (Math.random() * skew) - 0.2
            },
            colors: ['#ffd700'],
            shapes: ['circle'],
            scalar: randomInRange(0.4, 1)//размеры
        });
    
        if (timeLeft > 0) {
            requestAnimationFrame(frame);
        }
    }());    
}

function createCoin(){
    
    let coin_div = document.createElement('div');
    coin_div.style.width = "342px";
    coin_div.style.float = "left";
    coin_div.className = "side-blocks";    

    let score_h = document.createElement("H1");
    score_h.id = "text";
    score_h.style.color = "white";
    score_h.style.position = 'fixed';
    score_h.style.marginLeft = "5px";
    score_h.style.marginTop = "400px";
    score_h.addEventListener('click', changeText, false);
    function changeText(){
        score_h.textContent = "Money: " + priceList(money);
    }

    let coin= document.createElement('img');
    coin.src = 'images/coin.gif';
    coin.addEventListener('load', LoadCoin, false);
    function LoadCoin() {
        coin.setAttribute('width', '200px');
        coin.setAttribute('height', '200px');
        coin.setAttribute('alt', 'Coin');
        coin.setAttribute('id', 'coin');
        coin.setAttribute('z-index', '2');
        coin.style.display = 'block';
        coin.style.margin = "auto";
        coin.style.marginTop = "100px";
        coin.addEventListener('click', addMoney, false);
        function addMoney(){
            money = money + 1;
            console.log(money);
            document.getElementById("text").dispatchEvent(new Event("click"));            
        }
        setTimeout(function run() {
            money = money + passiveEarn;
            document.getElementById("text").dispatchEvent(new Event("click"));
            if(passiveEarn>0)coins();
            setTimeout(run, 2000);
          }, 10);
    }
    coin_div.appendChild(score_h);
    coin_div.appendChild(coin);
    return coin_div;
}
function createMap(){
    let map_div = document.createElement('div');
    map_div.style.float = "center";
    map_div.setAttribute("id","map");
    map_div.style.height = "100vh";
    map_div.style.marginRight = "342px";
    map_div.style.marginLeft= "342px";
    return map_div;
}
window.onload = function () {
    let window_div = document.createElement('div');
    window_div.appendChild(createCoin());
    window_div.appendChild(createShop());
    window_div.appendChild(createMap());

    document.body.appendChild(window_div);
}